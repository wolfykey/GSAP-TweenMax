'use strict'
import 'bootstrap-4-grid'
import 'normalize.css'
import './scss/main.scss'

var swiper = new Swiper('.swiper-container', {
  slidesPerView: 3,
  centeredSlides: true,
  loop: true,
  spaceBetween: 30,
  pagination: {
    el: '.swiper-pagination',
    clickable: true
  }
})
